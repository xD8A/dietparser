# coding: utf8

import html.parser

class CategoryParser(html.parser.HTMLParser):

    """
    Parses data from main page from health-diet.ru.

    Data format of subcategories: (category, subcategory, link).
    """

    HEAD="b-nav-main-calorie-food__item_text_head"
    SUB="b-nav-main-calorie-food__item_text_sub"
    NOT_SET, IS_HEAD, IS_SUB = range(3)

    def feed(self, data):
        self.state = self.NOT_SET
        self.subcategories = []
        super(CategoryParser, self).feed(data)

    def handle_starttag(self, tag, attrs):
        if tag=="a":
            attrdict = dict(attrs)
            classvalue = attrdict.get("class")
            if classvalue == self.HEAD:
                self.state = self.IS_HEAD
                self.link = attrdict["href"]
            elif classvalue == self.SUB:
                self.state = self.IS_SUB
                self.link = attrdict["href"]

    def handle_endtag(self, tag):
        if self.state != self.NOT_SET:
            self.state = self.NOT_SET

    def handle_data(self, data):
        if self.state == self.IS_HEAD:
            self.category = data
            self.subcategories.append((self.category, data, self.link))
        elif self.state == self.IS_SUB:
            self.subcategories.append((self.category, data, self.link))

class SubCategoryParser(html.parser.HTMLParser):

    """
    Parses subcategory page from health-diet.ru.

    Data format of products: (product, link).
    """

    def feed(self, data):
        self.istable = False
        self.islink = False
        self.products = []
        super(SubCategoryParser, self).feed(data)

    def handle_starttag(self, tag, attrs):
        if tag=="table":
            attrdict = dict(attrs)
            if attrdict.get("class")=="content":
                self.istable = True
        elif self.istable and tag=="a":
            attrdict = dict(attrs)
            self.islink = True
            self.link = attrdict["href"]

    def handle_endtag(self, tag):
        if self.islink:
            self.islink = False
        if self.istable and tag=="table":
            self.istable = False

    def handle_data(self, data):
        if self.islink:
            self.products.append((data, self.link))

class ProductParser(html.parser.HTMLParser):

    """
    Parses product page from health-diet.ru.

    Data format of content: (material, units, count).
    """

    def feed(self, data):
        self.isblock = False
        self.ismaterial = False
        self.iscount = False
        self.isunits = False
        self.content = []
        super(ProductParser, self).feed(data)
    
    def handle_starttag(self, tag, attrs):
        if tag=="div":
            attrdict = dict(attrs)
            classvalue = attrdict.get("class")
            if classvalue=="cnt_main_block2":
                self.isblock = True
            elif self.isblock:
                if classvalue=="cnt_count":
                    self.iscount = True
                    self.count = ""
                elif classvalue=="cnt_title":
                    self.ismaterial = True
                    self.material = ""

    def handle_endtag(self, tag):
        if self.isblock and tag=="td":
            self.isblock = False
        if self.ismaterial and tag=="div":
            self.ismaterial = False
        if self.iscount and tag=="div":
            self.iscount = False
            count, units = self.count.split()
            self.content.append((self.material, units, count))

    def handle_data(self, data):
        if self.ismaterial:
            self.material += data
        if self.iscount:
            self.count+=data
    

if __name__ == "__main__":

    import urllib.request
    import urllib.parse
    import urllib.error
    import sqlite3
    import os
    import time


    SITE = "http://health-diet.ru"
    DATABASE = "products.db"

    with urllib.request.urlopen(SITE) as f:
        html = f.read().decode("utf-8")

    parser = CategoryParser()
    parser.feed(html)
    subcategories = parser.subcategories

    categories = [a[0] for a in subcategories]
    subcategories = filter(lambda a: categories.count(a[0])==1 or a[0]!=a[1],
                           subcategories)

    conn = sqlite3.connect(DATABASE)
    cur = conn.cursor()
    try:
        cur.execute("CREATE TABLE IF NOT EXISTS products"
                    + " (category text,"
                    + "  subcategory text,"
                    + "  product text,"
                    + "  material text,"
                    + "  units text,"
                    + "  count real);")
        conn.commit()

        for category, subcategory, link in subcategories:

            link = urllib.parse.urljoin(SITE, link)

            while True:
                try:
                    with urllib.request.urlopen(link) as f:
                        html = f.read().decode("utf-8")
                except urllib.error.HTTPError:
                    time.sleep(10)
                    continue
                else:
                    break

            parser = SubCategoryParser()
            parser.feed(html)
            products = parser.products
    
            for product, link in products:
    
                print(product)

                sql = ("SELECT EXISTS(SELECT 1 FROM products WHERE"
                       +" (category='{0}') AND (subcategory='{1}')"
                       +" AND (product='{2}'))").format(
                           category, subcategory, product)
                cur.execute(sql)
                if cur.fetchone():
                    continue
    
                link = urllib.parse.urljoin(SITE, link)

                while True:
                    try:
                        with urllib.request.urlopen(link) as f:
                            html = f.read().decode("utf-8")
                    except urllib.error.HTTPError:
                        time.sleep(10)
                        continue
                    else:
                        break
                    
                parser = ProductParser()
                parser.feed(html)
                content = parser.content
    
                for material, units, count in content:
                    count = count.replace(',','.')
                    sql = ("INSERT INTO products VALUES ("
                           + "'{0}','{1}','{2}','{3}','{4}',{5}"
                           + ")").format(category, subcategory, product,
                                         material, units, count)
                    cur.execute(sql)
                    conn.commit()
    finally:
        conn.close()
            
    

            
